#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from nyc_bi.skeleton import fib

__author__ = "Christoph Scheidiger"
__copyright__ = "Christoph Scheidiger"
__license__ = "mit"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
