import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display, HTML
import seaborn as sns
import os

class InvoiceFile(object):
    def __init__(self, f_name, f_path=os.path.join('..','SmartBusiness','KPI')):
        self.input_f = os.path.join(f_path,f_name)
        self.bottle_cost_f = os.path.join(f_path, 'bottle_costs.csv')
        self.sales_channels = {'Webshop': [101, 111, 121], 'Direct': [104, 114, 124],
                               'Distributor': [100, 110, 120]}
        self.kombucha_position_numbers = [numb for _,lst in self.sales_channels.items() for numb in lst]
        self.flavors = {'BK':[100,101,104],'G':[110,111,114],
                        'QB':[120,121,124]}
        self.flavor_list = ['BK', 'G', 'QB']
        self.channel_list = ['Distributor','Webshop','Direct']
        self.time_interval = 'M'
        self.invoice_df = self._read_input()
        self.time_axis = pd.date_range(self.invoice_df.date.min(), self.invoice_df.date.max(),
                                       freq=self.time_interval)
        self.bottle_cost_df = self._read_bottle_costs()
        self.invoice_df_cost = self._calculate_costs(self.invoice_df)

        self.invoice_df_filt = self._filter_article_numbers(self.invoice_df_cost)

    @staticmethod
    def _transportation_costs(x, invoice_df):
        transportation_cost_dict = {100: 0.4, 110: 0.4, 120: 0.4, 101: 0.61, 111: 0.61, 121: 0.61, 104: 0.3, 114: 0.3,
                                    124: 0.3}
        invoice_n = x['number']
        df = invoice_df.loc[invoice_df['number'] == invoice_n]
        if 99.0 in df.position_number:
            transportation_paid = df.loc[df.position_number == 99.0]['position_amount']
        else:
            transportation_paid = 0
        pos_n = x['position_number']
        if pos_n in transportation_cost_dict.keys():
            quantity = x['position_amount']
            # print('quantity:', quantity, 'type:', type(quantity))
            # print('pos_n:', pos_n)
            # print('cost/bottle:', transportation_cost_dict[pos_n])
            # print('costs:', transportation_cost_dict[pos_n] * quantity)
            transportation_cost = transportation_cost_dict[pos_n] * quantity
            # if ~isinstance(transportation_cost, float):
            #     print(transportation_cost, type(transportation_cost))
            transportation_cost = transportation_cost - transportation_paid
        else:
            transportation_cost = 0

        return transportation_cost

    def _read_input(self):
        invoice_df = pd.read_csv(self.input_f, delimiter=';', parse_dates=[4, 6, 11], dayfirst=True)

        return invoice_df

    def _calculate_costs(self, invoice_df):
        bottle_costs_df = self._read_bottle_costs()
        invoice_df = invoice_df.merge(bottle_costs_df, how='left', left_on=[invoice_df.date.dt.year,
                                                                            invoice_df.date.dt.month],
                                        right_on=['year', 'month'])
        # calculate transportation costs
        invoice_df['costs_transportation'] = invoice_df.apply(lambda x: self._transportation_costs(x, invoice_df),
                                                                axis=1)
        invoice_df['costs_bottle'] = invoice_df['Bottle Price'] * invoice_df['position_amount']
        invoice_df['costs_total'] = invoice_df['costs_bottle'] + invoice_df['costs_transportation']

        return invoice_df

    def _filter_article_numbers(self, invoice_df):
        filt_invoice_df = invoice_df.loc[invoice_df.position_number.isin(self.kombucha_position_numbers)]

        return filt_invoice_df

    def _read_bottle_costs(self):
        # read bottle cost file
        bottle_cost_ser = pd.read_csv(self.bottle_cost_f, delimiter=',', parse_dates=[0], dayfirst=False)
        # display(bottle_cost_ser)
        # add bottle cost sample for latest date in invoices
        latest_cost = pd.DataFrame({'Date': [self.time_axis.max()],
                                    'Bottle Price': [bottle_cost_ser['Bottle Price'].iloc[-1]]})

        # display(latest_cost)

        bottle_cost_ser = pd.concat([bottle_cost_ser, latest_cost], axis=0)
        bottle_cost_ser['Date'] = pd.to_datetime(bottle_cost_ser['Date'], format='%d%b%Y')
        bottle_cost_ser = bottle_cost_ser.set_index('Date')

        # resample series
        bottle_costs_df = bottle_cost_ser.resample(self.time_interval).ffill()

        # display(bottle_costs_df)

        bottle_costs_df['year'] = bottle_costs_df.index.year
        bottle_costs_df['month'] = bottle_costs_df.index.month

        return bottle_costs_df

    def total_df(self):
        invoices = self.invoice_df_filt
        total_df = invoices.groupby([invoices.date.dt.year, invoices.date.dt.month])[
            ['position_amount', 'position_total', 'costs_total']] \
            .agg(np.nansum).copy()
        total_df = total_df.rename(columns={'position_amount': 'total_bottles', 'position_total': 'total_revenue',
                                            'costs_total': 'total_costs'})
        total_df.index.names = ['year', 'month']
        total_df.reset_index(inplace=True)
        total_df['total_profit'] = total_df['total_revenue'] - total_df['total_costs']

        return total_df

    def flavor_df(self):
        invoices = self.invoice_df_filt

        for fl in self.flavor_list:
            col_names = ['bottels_sold_' + fl.lower(), 'revenue_' + fl.lower(), 'costs_' + fl.lower()]
            # display(invoices.loc[invoices.position_number.isin(self.flavors[fl])])
            resTmp = invoices.loc[invoices.position_number.isin(self.flavors[fl])]\
                .groupby([invoices.date.dt.year, invoices.date.dt.month])[['position_amount','position_total','costs_total']]\
                .agg(np.nansum).copy()
            resTmp.index.names = ['year', 'month']
            resTmp.columns = col_names
            resTmp['profit_' + fl.lower()] = resTmp[col_names[1]] - resTmp[col_names[2]]
            resTmp.reset_index(inplace=True)

            if fl is 'BK':
                res = resTmp
            else:
                res = pd.merge(res, resTmp, how='left', on=['year', 'month'])

        return res

    def create_summary_df(self):
        total_df = self.total_df()
        flavor_df = self.flavor_df()
        summary_df = total_df.merge(flavor_df, how='left', on=['year', 'month'])
        return summary_df


